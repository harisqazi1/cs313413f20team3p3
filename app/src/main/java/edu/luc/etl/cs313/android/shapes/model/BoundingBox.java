package edu.luc.etl.cs313.android.shapes.model;

import android.content.Loader;
import android.graphics.Rect;

import java.util.List;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle)

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
		final Location loc = f.getShape().accept(this);
		int x = loc.getX();
		int y = loc.getY();
		final Rectangle	Rec = (Rectangle) loc.getShape();
		return  new Location(x, y, Rec);
	}

	@Override
	public Location onGroup(final Group g) {

		List<? extends Shape> shapeList = g.getShapes();
		Location onGroupBox = shapeList.get(0).accept(this); // the first bounding box
		Rectangle r   = (Rectangle) onGroupBox.getShape();
		int minumumX = onGroupBox.getX();
		int minumumY = onGroupBox.getY();
		int maximumX = minumumX + r.getWidth();
		int maximumY = minumumY + r.getHeight();
		for (int i = 1; i < shapeList.size(); i++) {
			Location onGroupLoopBox = shapeList.get(i).accept(this); // the next bounding box
			Rectangle nextR = (Rectangle)onGroupLoopBox.getShape();
			int minNextX = onGroupLoopBox.getX();
			int minNextY = onGroupLoopBox.getY();
			int maxNextX = minNextX + nextR.getWidth();
			int maxNextY = minNextY + nextR.getHeight();
			if (minNextX < minumumX){
				minumumX = minNextX;
			}
			if (minNextY < minumumY){
				minumumY = minNextY;
			}
			if (maximumX < maxNextX){
				maximumX = maxNextX;
			}
			if(maximumY < maxNextY){
				maximumY = maxNextY;
			}
		}

		return new Location(minumumX, minumumY, new Rectangle(maximumX - minumumX, maximumY - minumumY));
	}

	@Override
	public Location onLocation(final Location l) {//work on this, maybe similar to others, similar to onstrokecolor or onoutline with HINT 2 lines
		Location on_loc = l.getShape().accept(this);
		int lx = l.getX();
		int ly = l.getY();
		int x = lx + on_loc.getX();
		int y = ly + on_loc.getY();
		final Rectangle Rec = (Rectangle) on_loc.getShape();
		return new Location(x,y, Rec);
	}

	@Override
	public Location onRectangle(final Rectangle r) {//check it
		final int rHeight = r.getHeight();
		final int rWidth = r.getWidth();
		return new Location(0,0, new Rectangle(rWidth, rHeight));
	}

	@Override
	public Location onStrokeColor(final StrokeColor c) {
		final Location stroke_loc = c.getShape().accept(this);
		int x = stroke_loc.getX();
		int y = stroke_loc.getY();
		final Rectangle Rec = (Rectangle) stroke_loc.getShape();
		return new Location(x,y,Rec);
	}

	@Override
	public Location onOutline(final Outline o) {
		final Location loc = o.getShape().accept(this);
		int x = loc.getX();
		int y = loc.getY();
		final Rectangle	Rec = (Rectangle) loc.getShape();
		return  new Location(x, y, Rec);
	}

	@Override
	public Location onPolygon(final Polygon s) { //work on this
		return onGroup(s);

	}
}
