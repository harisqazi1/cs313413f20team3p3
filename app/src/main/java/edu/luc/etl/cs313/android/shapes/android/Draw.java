package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.os.Build;
import java.util.List;

import edu.luc.etl.cs313.android.shapes.model.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	// TODO entirely your job (except onCircle)

	private final Canvas canvas;

	private final Paint paint;


	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas; // FIXME - Changed from null to canvas
		this.paint = paint; // FIXME - Changed from null to paint
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
	}

	@Override
	public Void onStrokeColor(final StrokeColor c) {
		int previous_color = paint.getColor();
		Style paint_style = paint.getStyle();
		int color = c.getColor();
		paint.setColor(color);
		c.getShape().accept(this);
		paint.setColor(previous_color);
		paint.setStyle(paint_style);
		return null;
	}

	@Override
	public Void onFill(final Fill f) {

		Style paint_style = paint.getStyle();
		paint.setStyle(Style.FILL_AND_STROKE); //Fill the paint
		f.getShape().accept(this);
		paint.setStyle(paint_style);
		return null;
	}

	@Override
	public Void onGroup(final Group g) {
		for(Shape s: g.getShapes()){
			s.accept(this);
		}
		return null;
	}

	@Override
	public Void onLocation(final Location l) { //uses canvas object //worked on this
		int x = l.getX();
		int y = l.getY();
		canvas.translate(x,y);
		l.getShape().accept(this);
		canvas.translate(-x, -y);
		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {
		canvas.drawRect(0, 0, r.getWidth(), r.getHeight(), paint);
		return null;
	}

	@Override
	public Void onOutline(Outline o) {
		Style paint_style = paint.getStyle();
		paint.setStyle(Style.STROKE);
		o.getShape().accept(this);
		paint.setStyle(paint_style);
		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {
		List<? extends Point> PolygonPoints = s.getPoints(); //int PolygonPoints = (s.getPoints().size()); //instead create a list of points which = s.getPoints)
		final float[] pts = new float [4* PolygonPoints.size()]; //Polygon Points, change this to list.size
		//create an int j = 0;
		//use the j index as well as the i index
		//i is only valid in the for loop
		//j is for outside as well
		int j = 0;
		for (Point point : PolygonPoints) {
			pts[j++] = point.getX();
			pts[j++] = point.getY();
			if (j > 2) {
				pts[j++] = point.getX();
				pts[j++] = point.getY();
			}
		}
		pts[j++] = pts[0];
		pts[j] = pts[1];



		canvas.drawLines(pts, paint);
		return null;
	}
}
