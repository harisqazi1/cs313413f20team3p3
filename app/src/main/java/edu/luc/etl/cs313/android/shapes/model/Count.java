package edu.luc.etl.cs313.android.shapes.model;

import java.util.List;

/**
 * A visitor to compute the number of basic shapes in a (possibly complex)
 * shape.
 */
public class Count implements Visitor<Integer> {

	// TODO entirely your job

	@Override
	public Integer onPolygon(final Polygon p) {
		return 1;
	}

	@Override
	public Integer onCircle(final Circle c) {
		return 1;
	}

	@Override
	public Integer onGroup(final Group g) {// work on this
		int number_leafs = 0;
		List<? extends Shape> x = g.getShapes(); //Showed me that this could be a possible correction for my mistake
		for (Shape shape: x){
			number_leafs += shape.accept(this);
		}
		//for loop
		//iterate overall shapes
		//add to the leafs object to count within group
		return number_leafs;
	}

	@Override
	public Integer onRectangle(final Rectangle q) {
		return 1;
	}

	@Override
	public Integer onOutline(final Outline o) {
		return o.getShape().accept(this); //ask what this line does
	}

	@Override
	public Integer onFill(final Fill c) {
		return c.getShape().accept(this);
	}

	@Override
	public Integer onLocation(final Location l) {
		return l.getShape().accept(this);
	}

	@Override
	public Integer onStrokeColor(final StrokeColor c) {
		return c.getShape().accept(this);
	}
}
